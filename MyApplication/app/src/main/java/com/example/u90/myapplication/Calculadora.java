package com.example.u90.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Calculadora extends AppCompatActivity {
    //Nuemeros
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btn0;
    //Operaciones
    private Button btnIgual;
    private Button btnCancelar;
    private Button btnSuma;
    private Button btnResta;
    private Button btnMultiplicacion;
    private Button btnDivision;
    //texto de resulatdo
    private EditText resultado;
    //Operacion que desea ejecutar
    private String operacionEjecutar;
    //Datos
    private double datoA=0;
    private double datoB=0;
    private double total=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        btn0=findViewById(R.id.btn0);
        btn1=findViewById(R.id.btn1);
        btn2=findViewById(R.id.btn2);
        btn3=findViewById(R.id.btn3);
        btn4=findViewById(R.id.btn4);
        btn5=findViewById(R.id.btn5);
        btn6=findViewById(R.id.btn6);
        btn7=findViewById(R.id.btn7);
        btn8=findViewById(R.id.btn8);
        btn9=findViewById(R.id.btn9);

        btnIgual=findViewById(R.id.btnIgual);
        btnSuma=findViewById(R.id.btnSuma);
        btnResta=findViewById(R.id.btnResta);
        btnDivision=findViewById(R.id.btnDivision);
        btnMultiplicacion=findViewById(R.id.btnMultiplicacion);
        resultado =findViewById(R.id.resultado);
        btnCancelar = findViewById(R.id.btnCancelar);


        //NUMEROS
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "0");
                }else{
                    resultado.setText("0");
                }
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "1");
                }else{
                    resultado.setText("1");
                }
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "2");
                }else{
                    resultado.setText("2");
                }
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "3");
                }else{
                    resultado.setText("3");
                }
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "4");
                }else{
                    resultado.setText("4");
                }
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "5");
                }else{
                    resultado.setText("5");
                }
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "6");
                }else{
                    resultado.setText("6");
                }
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "7");
                }else{
                    resultado.setText("7");
                }
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "8");
                }else{
                    resultado.setText("8");
                }
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    resultado.setText(resultado.getText().toString() + "9");
                }else{
                    resultado.setText("9");
                }
            }
        });

        //OPERACIONES


        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    setOperacionEjecutar("S");
                    datoA = Double.parseDouble(resultado.getText().toString());
                    resultado.setText(resultado.getText().toString() + "+");
                }
            }
        });
        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    setOperacionEjecutar("R");
                    datoA = Double.parseDouble(resultado.getText().toString());
                    resultado.setText(resultado.getText().toString() + "-");
                }
               // ejecutarOperacion(operacionEjecutar);
            }
        });
        btnMultiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    setOperacionEjecutar("M");
                    datoA = Double.parseDouble(resultado.getText().toString());
                    resultado.setText(resultado.getText().toString() + "X");
                }
                //ejecutarOperacion(operacionEjecutar);
            }
        });
        btnDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultado.getText().toString().trim().isEmpty()) {
                    datoA = Double.parseDouble(resultado.getText().toString());
                    setOperacionEjecutar("D");
                    resultado.setText(resultado.getText().toString() + "/");
                }
               // ejecutarOperacion(operacionEjecutar);
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               limpiar();
            }
        });

        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerDatoB(getOperadorEjecutar());
                ejecutarOperacion();
            }
        });
    }



    private void  setOperacionEjecutar(String operacion){
        this.operacionEjecutar = operacion;
    }

    private String getOperacionEjecutar(){
        return this.operacionEjecutar;
    }
    private void ejecutarOperacion (){

        switch (getOperacionEjecutar() ){
            case "S":
                total = datoA + datoB;
                resultado.setText(String.valueOf(total));
            break;
            case "R":
                total = datoA - datoB;
                resultado.setText(String.valueOf(total));
            break;
            case "M":
                total = datoA * datoB;
                resultado.setText(String.valueOf(total));
                break;
            case "D":
                total = datoA / datoB;
                resultado.setText(String.valueOf(total));
             break;

            default:
        }
    }

    private void limpiar(){
        datoA = 0;
        datoB = 0;
        operacionEjecutar = "";
        resultado.setText("");
    }

    private String getOperadorEjecutar(){

        String operador = "";

        switch (getOperacionEjecutar() ) {
            case "S":
                operador = "\\+";
                break;
            case "R":
                operador = "-";
                break;
            case "M":
                operador = "X";
                break;
            case "D":
                operador = "/";
                break;

            default:
        }

        return operador;

    }

    private void obtenerDatoB(String ope){
        String dato[] = resultado.getText().toString().split(ope);
        if(dato.length > 1)
         datoB = Double.parseDouble(dato[1]);
    }
}
