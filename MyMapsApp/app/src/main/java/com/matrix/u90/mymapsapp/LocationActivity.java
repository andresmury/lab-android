package com.matrix.u90.mymapsapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class LocationActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISION_CODE = 34;
    private TextView textLongitud ;
    private TextView textLatitud;
    protected Location mLastLocation;
    private FusedLocationProviderClient mfusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        textLatitud = findViewById(R.id.textLatitud);
        textLongitud = findViewById(R.id.textLongitud);
        mfusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!checkPermissions()){
            requestPermissions();
        }else{
            getLasLocation();
        }
    }

    private void requestPermissions() {
        boolean showProviderRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION);

        if(showProviderRationale){
            //TODO: solicitar al usuario encender GPS
            Log.i("Location","Mensaje para el usuario , favor encender el GPS");
        }else{
            startLocationPermissionRequest();
        }

    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,new String []{Manifest.permission.ACCESS_COARSE_LOCATION},REQUEST_PERMISION_CODE);
    }

    @SuppressLint("MissingPermission")
    private void getLasLocation() {

        mfusedLocationProviderClient.getLastLocation().addOnCompleteListener(this, new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if(task.isSuccessful() && task.getResult() !=null){
                    mLastLocation = task.getResult();
                    textLatitud.setText(String.valueOf(mLastLocation.getLatitude()));
                    textLongitud.setText(String.valueOf(mLastLocation.getLongitude()));
                }else{
                    Toast.makeText(LocationActivity.this,"Localizacion no encontrada",Toast.LENGTH_LONG ).show();
                }
            }
        });
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_PERMISION_CODE){
            if(grantResults.length <= 0){
                Log.i("Location","el usuario cancelo el permiso");
            }else if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                getLasLocation();
            }else{
                Log.i("Location","usuario denegó el permiso");
            }
        }
    }
}
