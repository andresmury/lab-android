package com.matrix.u90.mymapsapp.Services;

import com.matrix.u90.mymapsapp.models.Cinema;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IService {

    @GET("cinemas")
    Call<ArrayList<Cinema>> getCinemas();
}
