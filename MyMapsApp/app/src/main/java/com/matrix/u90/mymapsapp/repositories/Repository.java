package com.matrix.u90.mymapsapp.repositories;

import com.matrix.u90.mymapsapp.Services.IService;
import com.matrix.u90.mymapsapp.Services.ServicesFactory;
import com.matrix.u90.mymapsapp.models.Cinema;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class Repository {
    private IService iService;

    public Repository(){
        ServicesFactory servicesFactory = new ServicesFactory();
        iService = (IService) servicesFactory.getInstanceServices(IService.class);
    }

    public ArrayList<Cinema> getCinemas() throws IOException {

        try{
            Call<ArrayList<Cinema>> call = iService.getCinemas();
            Response <ArrayList<Cinema>> response =  call.execute();
            if(response.errorBody()!=null){
                throw defaultError();
            }else{
                return response.body();
            }

        }catch(IOException e){
            throw defaultError();
        }
    }

    private IOException defaultError() {
        return new IOException("Ha ocurrido un error");
    }
}
