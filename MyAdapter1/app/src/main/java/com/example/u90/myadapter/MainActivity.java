package com.example.u90.myadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private AdapterProductos adapterProductos;
    private AdapterMercado adapterMercado;
    private RecyclerView recyclerView;
    private RecyclerView recyclerViewMercado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerViewMercado= findViewById(R.id.recyclerViewMercado);
        loadAdapters();
    }

    private void loadAdapters() {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        ArrayList<Producto> listaMercado = new ArrayList<Producto>();
        Producto producto = new Producto();
        producto.setProductoNombre("Renault");
        producto.setProductoDescripcion("Sandero");
        lista.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Nissan");
        producto.setProductoDescripcion("Sentra");
        lista.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Toyota");
        producto.setProductoDescripcion("Corola");
        lista.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Mazda");
        producto.setProductoDescripcion("Allegro");
        lista.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Kia");
        producto.setProductoDescripcion("Rio");
        lista.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Audi");
        producto.setProductoDescripcion("El mejor");
        lista.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Sckoda");
        producto.setProductoDescripcion("Sedan");
        lista.add(producto);

        producto = new Producto();
        producto.setProductoNombre("Cafe");
        producto.setProductoDescripcion("Bastilla");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Arroz");
        producto.setProductoDescripcion("Diana");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Azucar");
        producto.setProductoDescripcion("Incauca");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Espaguetis");
        producto.setProductoDescripcion("La Muñeca");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Sal");
        producto.setProductoDescripcion("Refisal");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Chocolate");
        producto.setProductoDescripcion("Corona");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Limpido");
        producto.setProductoDescripcion("Clorox");
        listaMercado.add(producto);

        adapterProductos = new AdapterProductos(lista);
        adapterMercado = new AdapterMercado(listaMercado);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterProductos);

        LinearLayoutManager linearLayoutManagerMerc = new LinearLayoutManager(this);
        recyclerViewMercado.setLayoutManager(linearLayoutManagerMerc);
        recyclerViewMercado.setAdapter(adapterMercado);

    }
}
