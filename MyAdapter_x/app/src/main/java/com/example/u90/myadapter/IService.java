package com.example.u90.myadapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IService {


    @GET("producto")
    Call<ArrayList<Producto>> getProductos();
}
