package com.example.u90.myadapter.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.u90.myadapter.fragments.OneFragment;
import com.example.u90.myadapter.R;
import com.example.u90.myadapter.fragments.TwoFragment;

public class Main2Activity extends AppCompatActivity {


    private TextView textViewOne;
    private TextView textViewTwo;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textViewOne = findViewById(R.id.textViewOne);
        textViewTwo = findViewById(R.id.textViewTwo);
        frameLayout = findViewById(R.id.frameLayout);

       // showFragment(new TwoFragment());

        textViewOne.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                showFragment(new OneFragment());
               }
           }
        );

        textViewTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                     showFragment(new TwoFragment());
            }
        });
    }

    private void showFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(frameLayout.getId(),fragment);
        fragmentTransaction.commit();

    }
}
