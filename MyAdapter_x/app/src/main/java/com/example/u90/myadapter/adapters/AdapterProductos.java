package com.example.u90.myadapter.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.u90.myadapter.models.Producto;
import com.example.u90.myadapter.R;

import java.util.ArrayList;

public class AdapterProductos extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<Producto> listaProductos;

    public AdapterProductos(ArrayList<Producto> listProductos) {
        this.listaProductos = listProductos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_productos,viewGroup,false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
         CustomViewHolder customViewHolder=(CustomViewHolder) viewHolder;
         Producto product = listaProductos.get(position);
         customViewHolder.textViewName.setText(product.getProductoNombre());
         customViewHolder.textViewDescription.setText(product.getProductoDescripcion());
    }

    @Override
    public int getItemCount() {
        return listaProductos.size();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder{

        private TextView textViewName;
        private TextView textViewDescription;

        public CustomViewHolder(View itemView) {
            super(itemView);
            textViewName=itemView.findViewById(R.id.textViewName);
            textViewDescription= itemView.findViewById(R.id.textViewDescripcion);
        }
    }
}
