package com.example.u90.myadapter.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.u90.myadapter.R;
import com.example.u90.myadapter.activities.CreateProductActivity;
import com.example.u90.myadapter.adapters.AdapterMercado;
import com.example.u90.myadapter.helper.ValidateInternet;
import com.example.u90.myadapter.models.Producto;
import com.example.u90.myadapter.repositories.Repository;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.u90.myadapter.R.id.buttonCreateProduct;


/**
 * A simple {@link Fragment} subclass.
 */
public class TwoFragment extends Fragment {


    private RecyclerView recyclerViewMercado;
    private AdapterMercado adapterMercado;
    private Repository repository;
    private Button buttonCreateProduct;

    public TwoFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_two, container, false);
        recyclerViewMercado= view.findViewById(R.id.recyclerViewMercado);
        buttonCreateProduct = view.findViewById(R.id.buttonCreateProduct);
        repository = new Repository();
        validateInternet();

       buttonCreateProduct.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Intent intent = new Intent(getActivity(),CreateProductActivity.class);
                  startActivity(intent);
              }
          }

       );
        return view;

    }


    private void loadAdapters(final ArrayList<Producto> productos) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterMercado = new AdapterMercado(productos);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                recyclerViewMercado.setLayoutManager(linearLayoutManager);

                LinearLayoutManager linearLayoutManagerMerc = new LinearLayoutManager(getContext());
                recyclerViewMercado.setLayoutManager(linearLayoutManagerMerc);
                recyclerViewMercado.setAdapter(adapterMercado);

            }
        });
    }


        private void getProductos(){

            try{
                ArrayList<Producto> productos = repository.getProductos();
                loadAdapters(productos);

            }catch (final IOException e){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

            }
        }

     private void createThreadGetProducto(){
         Thread thread = new Thread(new Runnable() {
             @Override
             public void run() {
                 getProductos();
             }
         });
         thread.start();
     }


    @Override
    public void onResume() {
        super.onResume();
        validateInternet();

    }

    private void validateInternet(){
        final ValidateInternet validateInternet = new ValidateInternet(getContext());
          if(validateInternet.isConnected())  {
              createThreadGetProducto();
          }else{
              AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
              alertDialog.setTitle(R.string.title_validate_internet);
              alertDialog.setMessage(R.string.message_validate_internet);
              alertDialog.setCancelable(false);
              alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                      validateInternet();
                      dialogInterface.dismiss();
                  }
              });
              alertDialog.show();
          }



    }
}
