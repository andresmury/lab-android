package com.example.u90.myadapter.services;

import com.example.u90.myadapter.models.Producto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface IService {


    @GET("products")
    Call<ArrayList<Producto>> getProductos();

    @POST("products")
    Call<Producto> saveProducto(@Body Producto producto);
}
