package com.example.u90.myadapter.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.u90.myadapter.R;
import com.example.u90.myadapter.models.Producto;
import com.example.u90.myadapter.repositories.Repository;

import java.io.IOException;

public class CreateProductActivity extends AppCompatActivity implements TextWatcher {

    private EditText product_etDescripcion;
    private EditText product_etPrecio;
    private EditText product_etCantidad;
    private EditText product_etMarca;
    private EditText product_etNombre;
    private Button buttonCreateProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_product);
        loadView();
        loadEvents();
    }


    private void loadEvents(){
        buttonCreateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Producto producto = new Producto();
                producto.setProductoNombre(product_etNombre.getText().toString());
                producto.setProductoDescripcion(product_etDescripcion.getText().toString());
                producto.setPrecio(Integer.parseInt(product_etPrecio.getText().toString()));
                producto.setMarca(product_etMarca.getText().toString());
                producto.setCantidad(Integer.parseInt(product_etCantidad.getText().toString()));
                createThreadCreateProduct(producto);

            }
        });

    }

    private void createThreadCreateProduct(final Producto producto){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createProduct(producto);
            }
        });
        thread.start();
    }

    private void createProduct(Producto producto) {

        Repository repository = new Repository();
        try {
            repository.saveProducto(producto);
            finish();
        }catch (final IOException e){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(CreateProductActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void loadView(){
        product_etNombre = findViewById(R.id.product_etNombre) ;
        product_etNombre.addTextChangedListener(this);
        product_etDescripcion = findViewById(R.id.product_etDescripcion);
        product_etDescripcion.addTextChangedListener(this);
        product_etPrecio= findViewById(R.id.product_etPrecio);
        product_etPrecio.addTextChangedListener(this);
        product_etCantidad= findViewById(R.id.product_etCantidad);
        product_etCantidad.addTextChangedListener(this);
        product_etMarca = findViewById(R.id.product_etMarca);
        product_etMarca.addTextChangedListener(this);
        buttonCreateProduct = findViewById(R.id.buttonCreateProduct);
    }

    private boolean isEmptyAnyEditText(){
        return (isEmptyEditText(product_etNombre) || isEmptyEditText(product_etCantidad) ||
                isEmptyEditText(product_etDescripcion) || isEmptyEditText(product_etMarca)|| isEmptyEditText(product_etPrecio) );

    }

    private boolean isEmptyEditText(EditText edit){
        return edit.getText().toString().trim().isEmpty();
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (isEmptyAnyEditText()){
            buttonCreateProduct.setEnabled(false);
            buttonCreateProduct.setBackgroundResource(R.color.colorWhite);
        }else{
            buttonCreateProduct.setEnabled(true);
            buttonCreateProduct.setBackgroundResource(R.color.colorPrimaryDark);
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }




}
