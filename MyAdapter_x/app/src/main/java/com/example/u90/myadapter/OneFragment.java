package com.example.u90.myadapter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment {

    private RecyclerView recyclerViewMercado;
    private AdapterMercado adapterMercado;
    private Repository repository;

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_one, container, false);
        recyclerViewMercado= view.findViewById(R.id.recyclerViewMercado);

        repository = new Repository();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getProductos();
            }
        });
        thread.start();
        return view;

    }

    private void loadAdapters(final ArrayList<Producto> productos) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterMercado = new AdapterMercado(productos);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                recyclerViewMercado.setLayoutManager(linearLayoutManager);

                LinearLayoutManager linearLayoutManagerMerc = new LinearLayoutManager(getContext());
                recyclerViewMercado.setLayoutManager(linearLayoutManagerMerc);
                recyclerViewMercado.setAdapter(adapterMercado);

            }
        });
        /*ArrayList<Producto> listaMercado = new ArrayList<Producto>();
        Producto producto = new Producto();
        producto.setProductoNombre("Cafe");
        producto.setProductoDescripcion("Bastilla");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Arroz");
        producto.setProductoDescripcion("Diana");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Azucar");
        producto.setProductoDescripcion("Incauca");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Espaguetis");
        producto.setProductoDescripcion("La Muñeca");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Sal");
        producto.setProductoDescripcion("Refisal");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Chocolate");
        producto.setProductoDescripcion("Corona");
        listaMercado.add(producto);
        producto = new Producto();
        producto.setProductoNombre("Limpido");
        producto.setProductoDescripcion("Clorox");
        listaMercado.add(producto);*/



    }

    private void getProductos(){

        try{
            ArrayList<Producto> productos = repository.getProductos();
            loadAdapters(productos);

        }catch (final IOException e){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }
}
