package com.matrix.u90.myappleague.interfaces;

import com.matrix.u90.myappleague.models.League;
import com.matrix.u90.myappleague.views.IBaseView;

import java.util.ArrayList;

public interface IMainView extends IBaseView {

    void loadAllLeagues(ArrayList<League> listLeagues);
}
