package com.matrix.u90.myappleague.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.matrix.u90.myappleague.R;
import com.matrix.u90.myappleague.adapters.AdapterEvent;
import com.matrix.u90.myappleague.interfaces.IDatosTeamView;
import com.matrix.u90.myappleague.models.Event;
import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.presenters.DatosTeamPresenter;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

public class DatosTeamActivity extends  BaseActivity<DatosTeamPresenter> implements IDatosTeamView  {

    private Team team;
    private TextView texViewDescritionTeam;
    private TextView textViewTeam;
    private ImageView imageTeamBanner;
    private ImageView imageTeamJersey;
    private ImageView imageTeamBadge;
    private ImageView imageViewYoutube;
    private ImageView imageViewFacebook;
    private ImageView imageViewTwitter;
    private RecyclerView recyclerView;
    private AdapterEvent adapterEvent;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_team);
        setComponents();
        loadComponents();
        setPresenter(new DatosTeamPresenter());
        getPresenter().inject(this, getValidateInternet());
        if(!getPresenter().getValidateInternet().isConnected()) {
            showToast("No hay conexion a internet");
        }else {
            loadEvents();
        }

    }

    private void loadEvents() {
            try{
                getPresenter().onLoadListEvents(team.getIdTeam());
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private void setComponents() {
        texViewDescritionTeam = findViewById(R.id.texViewDescritionTeam);
        imageTeamBadge =findViewById(R.id.imageTeamBadge);
        imageTeamJersey=findViewById(R.id.imageTeamJersey);
        imageTeamBanner=findViewById(R.id.imageTeamBanner);
        textViewTeam=findViewById(R.id.textViewTeam);
        imageViewYoutube=findViewById(R.id.imageViewYoutube);
        imageViewFacebook=findViewById(R.id.imageViewFacebook);
        imageViewTwitter = findViewById(R.id.imageViewTwitter);
        recyclerView = findViewById(R.id.recyclerEvents);
    }

    public void loadComponents(){

        team = (Team) getIntent().getSerializableExtra("teamSearch");
        textViewTeam.setText(team.getStrTeam());
        texViewDescritionTeam.setText(team.getStrDescriptionEN());
        //Obtener el idioma configurado en nuestro dispositivo
        if(getResources().getSystem().getConfiguration().locale.getDisplayName().contains("español")){
            Log.i("entra","idioma español");
            if(team.getStrDescriptionES()!=null && !"".equalsIgnoreCase(team.getStrDescriptionES())) {
                texViewDescritionTeam.setText(team.getStrDescriptionES());
            }
        }
        texViewDescritionTeam.setVerticalScrollBarEnabled(true);
        texViewDescritionTeam.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        texViewDescritionTeam.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        texViewDescritionTeam.setMovementMethod(ScrollingMovementMethod.getInstance());
        texViewDescritionTeam.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                view.getParent().requestDisallowInterceptTouchEvent(true);
                if ((motionEvent.getAction() & MotionEvent.ACTION_UP) != 0 && (motionEvent.getActionMasked() & MotionEvent.ACTION_UP) != 0)
                {
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                }
                return false;
            }
        });
        if(team.getStrTeamBanner()!=null && !"".equalsIgnoreCase(team.getStrTeamBanner()))
            Picasso.get().load(team.getStrTeamBanner()).into(imageTeamBanner);

        if(team.getStrTeamJersey()!=null && !"".equalsIgnoreCase(team.getStrTeamJersey()))
            Picasso.get().load(team.getStrTeamJersey()).into(imageTeamJersey);

        if(team.getStrTeamBadge()!=null && !"".equalsIgnoreCase(team.getStrTeamBadge()))
            Picasso.get().load(team.getStrTeamBadge()).into(imageTeamBadge);

        imageViewYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        openLink(team.getStrYoutube());
                    }
                });
            }
        });

        imageViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        openLink(team.getStrFacebook());
                    }
                });
            }
        });

        imageViewTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        openLink(team.getStrTwitter());
                    }
                });
            }
        });


    }
    @Override
    public void loadViewEventsTeam(final ArrayList<Event> listEvents) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterEvent = new AdapterEvent(listEvents,DatosTeamActivity.this);
                linearLayoutManager = new LinearLayoutManager(DatosTeamActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapterEvent);
            }
        });
    }

}
