package com.matrix.u90.myappleague.interfaces;

import com.matrix.u90.myappleague.models.Event;
import com.matrix.u90.myappleague.views.IBaseView;

import java.util.ArrayList;

public interface IDatosTeamView extends IBaseView {
    public void loadViewEventsTeam(ArrayList<Event> listEvents);
}
