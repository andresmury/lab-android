package com.matrix.u90.myappleague.interfaces;

import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.views.IBaseView;

import java.util.ArrayList;

public interface IDatosLigaView extends IBaseView {

    public void loadViewTeamsLigue(ArrayList<Team> listTeam);
    public void loadInfoTeam(Team team);
}
