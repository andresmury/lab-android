package com.matrix.u90.myappleague.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Team  implements Serializable {


    @SerializedName("idTeam")
    @Expose
    private String idTeam;

    @SerializedName("strTeam")
    @Expose
    private String strTeam;

    @SerializedName("strTeamShort")
    @Expose
    private String strTeamShort;

    @SerializedName("strSport")
    @Expose
    private String strSport;

    @SerializedName("strLeague")
    @Expose
    private String strLeague;

    @SerializedName("strTeamBadge")
    @Expose
    private String strTeamBadge;

    @SerializedName("strTeamJersey")
    @Expose
    private String strTeamJersey;

    @SerializedName("strDescriptionEN")
    @Expose
    private String strDescriptionEN;

    @SerializedName("strDescriptionES")
    @Expose
    private String strDescriptionES;

    @SerializedName("strStadium")
    @Expose
    private String strStadium;

    @SerializedName("strWebsite")
    @Expose
    private String strWebsite;

    @SerializedName("strFacebook")
    @Expose
    private String strFacebook;

    @SerializedName("strTwitter")
    @Expose
    private String strTwitter;

    @SerializedName("strInstagram")
    @Expose
    private String strInstagram;

    @SerializedName("strTeamBanner")
    @Expose
    private String strTeamBanner;

    @SerializedName("strYoutube")
    @Expose
    private String strYoutube;


    @SerializedName("strStadiumThumb")
    @Expose
    private String strStadiumThumb;

    @SerializedName("strStadiumLocation")
    @Expose
    private String strStadiumLocation;



    public String getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(String idTeam) {
        this.idTeam = idTeam;
    }

    public String getStrTeam() {
        return strTeam;
    }

    public void setStrTeam(String strTeam) {
        this.strTeam = strTeam;
    }

    public String getStrTeamShort() {
        return strTeamShort;
    }

    public void setStrTeamShort(String strTeamShort) {
        this.strTeamShort = strTeamShort;
    }

    public String getStrSport() {
        return strSport;
    }

    public void setStrSport(String strSport) {
        this.strSport = strSport;
    }

    public String getStrLeague() {
        return strLeague;
    }

    public void setStrLeague(String strLeague) {
        this.strLeague = strLeague;
    }

    public String getStrTeamBadge() {
        return strTeamBadge;
    }

    public void setStrTeamBadge(String strTeamBadge) {
        this.strTeamBadge = strTeamBadge;
    }

    public String getStrTeamJersey() {
        return strTeamJersey;
    }

    public void setStrTeamJersey(String strTeamJersey) {
        this.strTeamJersey = strTeamJersey;
    }

    public String getStrDescriptionEN() {
        return strDescriptionEN;
    }

    public void setStrDescriptionEN(String strDescriptionEN) {
        this.strDescriptionEN = strDescriptionEN;
    }

    public String getStrStadium() {
        return strStadium;
    }

    public void setStrStadium(String strStadium) {
        this.strStadium = strStadium;
    }

    public String getStrWebsite() {
        return strWebsite;
    }

    public void setStrWebsite(String strWebsite) {
        this.strWebsite = strWebsite;
    }

    public String getStrFacebook() {
        return strFacebook;
    }

    public void setStrFacebook(String strFacebook) {
        this.strFacebook = strFacebook;
    }

    public String getStrTwitter() {
        return strTwitter;
    }

    public void setStrTwitter(String strTwitter) {
        this.strTwitter = strTwitter;
    }

    public String getStrInstagram() {
        return strInstagram;
    }

    public void setStrInstagram(String strInstagram) {
        this.strInstagram = strInstagram;
    }
    public String getStrTeamBanner() {
        return strTeamBanner;
    }

    public void setStrTeamBanner(String strTeamBanner) {
        this.strTeamBanner = strTeamBanner;
    }

    public String getStrYoutube() {
        return strYoutube;
    }

    public void setStrYoutube(String strYoutube) {
        this.strYoutube = strYoutube;
    }

    public String getStrStadiumThumb() {
        return strStadiumThumb;
    }

    public void setStrStadiumThumb(String strStadiumThumb) {
        this.strStadiumThumb = strStadiumThumb;
    }

    public String getStrStadiumLocation() {
        return strStadiumLocation;
    }

    public void setStrStadiumLocation(String strStadiumLocation) {
        this.strStadiumLocation = strStadiumLocation;
    }

    public String getStrDescriptionES() {
        return strDescriptionES;
    }

    public void setStrDescriptionES(String strDescriptionES) {
        this.strDescriptionES = strDescriptionES;
    }


    @Override
    public String toString() {
        return ""+strTeam;
    }


}
