package com.matrix.u90.myappleague.Services;

import com.matrix.u90.myappleague.models.Events;
import com.matrix.u90.myappleague.models.League;
import com.matrix.u90.myappleague.models.Leagues;
import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.models.Teams;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IService {

    @GET("all_leagues.php")
    Call<Leagues> loadLeagues();


    @GET("lookup_all_teams.php")
    Call<Teams> loadTeams(@Query("id") String id);

    @GET("eventsnext.php")
    Call<Events> loadEventsTeam(@Query("id") String id);


}
