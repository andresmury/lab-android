package com.matrix.u90.myappleague.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Leagues  implements Serializable {

    @Expose
    @SerializedName("leagues")
    ArrayList<League> list;

    public ArrayList<League> getList() {
        return list;
    }

    public void setList(ArrayList<League> list) {
        this.list = list;
    }
}
