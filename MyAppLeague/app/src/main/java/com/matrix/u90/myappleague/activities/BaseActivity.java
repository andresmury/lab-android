package com.matrix.u90.myappleague.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.matrix.u90.myappleague.helper.ValidateInternet;
import com.matrix.u90.myappleague.presenters.BasePresenter;
import com.matrix.u90.myappleague.views.IBaseView;

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements IBaseView {

    private ValidateInternet validateInternet;
    private T presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validateInternet = new ValidateInternet(this);
    }

    public T getPresenter() {
        return presenter;
    }

    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    @Override
    public void showAlertDialog() {
        //TODO
    }

    @Override
    public void showToast(String mensaje) {
        Toast.makeText((Context) getPresenter().getView(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openLink(String link) {
        Uri uri = Uri.parse("http://"+link);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void openEnlace(String enlace) {
        Intent intent = new Intent((Context) getPresenter().getView(), WebViewActivity.class);
        intent.putExtra("url","http://"+enlace);
        startActivity(intent);
    }






}
