package com.matrix.u90.myappleague.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.matrix.u90.myappleague.R;

public class WebViewActivity extends Activity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        String url = getIntent().getStringExtra("url");
        webView = (WebView) findViewById(R.id.mywebView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        finish();

    }
}
