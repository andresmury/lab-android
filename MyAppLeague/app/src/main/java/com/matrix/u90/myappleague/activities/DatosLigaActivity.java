package com.matrix.u90.myappleague.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.matrix.u90.myappleague.R;
import com.matrix.u90.myappleague.adapters.AdapterTeam;
import com.matrix.u90.myappleague.interfaces.IDatosLigaView;
import com.matrix.u90.myappleague.models.League;
import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.presenters.DatosLigaPresenter;

import java.io.IOException;
import java.util.ArrayList;

public class DatosLigaActivity extends BaseActivity<DatosLigaPresenter> implements IDatosLigaView {

    ArrayList<League> listLeagues;
    League selectionLeague;
    Spinner spinner;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private AdapterTeam adapterTeam;
    private ProgressBar progresBarLiga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_liga);
        setComponents();
        loadComponents();
    }

    private void setComponents() {
        setPresenter(new DatosLigaPresenter());
        getPresenter().inject(this, getValidateInternet());
        spinner = findViewById(R.id.spinnerlegues);
        recyclerView = findViewById(R.id.recyclerTeams);
        progresBarLiga = findViewById(R.id.progresBarLiga);
    }

    private void loadComponents() {
        listLeagues = (ArrayList<League>) getIntent().getSerializableExtra("listaLigas");
        spinner.setAdapter(new ArrayAdapter<League>(DatosLigaActivity.this,android.R.layout.simple_spinner_item,listLeagues));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectionLeague = (League)parent.getItemAtPosition(position);
                progresBarLiga.setVisibility(View.VISIBLE);
                if(!getPresenter().getValidateInternet().isConnected()) {
                    showToast("No hay conexion a internet");
                }else{
                    loadTeams(selectionLeague.getIdLeague());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    public void loadTeams(String idLeague){
       try{
           getPresenter().onLoadLisTeams(idLeague);
         } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadViewTeamsLigue(final ArrayList<Team> listTeam) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterTeam  = new AdapterTeam(listTeam,DatosLigaActivity.this);
                linearLayoutManager = new LinearLayoutManager(DatosLigaActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapterTeam);
                progresBarLiga.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void loadInfoTeam(final Team teamSearch) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!getPresenter().getValidateInternet().isConnected()) {
                    showToast("No hay conexion a internet");
                } else{
                    Intent intent = new Intent(DatosLigaActivity.this, DatosTeamActivity.class);
                    intent.putExtra("teamSearch", teamSearch);
                    startActivity(intent);
                    //finish();
                    }
            }
        });
    }
}
