package com.matrix.u90.myappleague.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.matrix.u90.myappleague.R;
import com.matrix.u90.myappleague.interfaces.IDatosTeamView;
import com.matrix.u90.myappleague.models.Event;

import java.util.ArrayList;

public class AdapterEvent extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private  ArrayList<Event> listEvents;
    private IDatosTeamView iDatosTeamView;

    public AdapterEvent(ArrayList<Event> listEvents, IDatosTeamView iDatosTeamView) {
        this.listEvents = listEvents;
        this.iDatosTeamView = iDatosTeamView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event,parent,false);
        return new CustomViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        CustomViewHolder customViewHolder = (CustomViewHolder) holder;
        customViewHolder.textViewStrEvent.setText( listEvents.get(position).getStrEvent()+"  "+listEvents.get(position).getDateEvent());

    }

    @Override
    public int getItemCount() {
        return listEvents.size();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder{

    private TextView textViewStrEvent;

    public CustomViewHolder(View itemView) {
        super(itemView);
        textViewStrEvent= itemView.findViewById(R.id.textViewStrEvent);
    }
  }
}
