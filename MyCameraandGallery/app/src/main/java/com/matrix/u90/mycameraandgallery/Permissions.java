package com.matrix.u90.mycameraandgallery;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class Permissions {

    public static boolean isGrantedPermissions(Context context, String permissionsType){
        int permission = ActivityCompat.checkSelfPermission(context,permissionsType);
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    public static void verifyPermissions(Activity activity, String[] permissionsType){
        ActivityCompat.requestPermissions(activity,permissionsType,3);
    }
}
