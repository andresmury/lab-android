package com.matrix.u90.mycameraandgallery;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class PhotoAdadpter extends RecyclerView.Adapter<PhotoAdadpter.ViewHolder> {

    private ArrayList<String> arrayPhotos;
    private Context context;

    public void setArrayPhotos(ArrayList<String> arrayPhotos) {
        this.arrayPhotos = arrayPhotos;
    }


    @NonNull
    @Override
    public PhotoAdadpter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewHolder = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo,viewGroup,false);
        this.context=viewGroup.getContext();
        return new ViewHolder(viewHolder);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoAdadpter.ViewHolder viewHolder, int position) {
        String photo= arrayPhotos.get(position);
        Glide.with(this.context).load(photo.toString()).into(viewHolder.imagePhoto);

    }

    @Override
    public int getItemCount() {
        return arrayPhotos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private  ImageView imagePhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagePhoto = itemView.findViewById(R.id.imageItemPhoto);

        }
    }
}
