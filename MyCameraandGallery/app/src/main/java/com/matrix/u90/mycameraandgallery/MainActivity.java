package com.matrix.u90.mycameraandgallery;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private ImageView camera;
    private ImageView gallery;
    private RecyclerView recyclerView;
    private PhotoAdadpter photoAdadpter;
    private ArrayList<String> photos;
    private File photoFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();


}

    private void initComponents() {
        camera= findViewById(R.id.mycamera);
        gallery= findViewById(R.id.mygallery);
        recyclerView= findViewById(R.id.myrecycler);
        photos = new ArrayList<>();

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGallery();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCamera();
            }
        });
        callAdapter();
    }





    private void callAdapter() {
       RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
       recyclerView.setLayoutManager(layoutManager);
       photoAdadpter= new PhotoAdadpter();
       photoAdadpter.setArrayPhotos(photos);
       recyclerView.setAdapter(photoAdadpter);



    }

    private void showCamera() {
        if( Permissions.isGrantedPermissions(this,Manifest.permission.CAMERA)){
            openCamera();
        }else
        {
            String [] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA};
            Permissions.verifyPermissions(this,permissions);
        }
    }
    private void showGallery(){
       if( Permissions.isGrantedPermissions(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
           openGallery();
       }else
       {
           String [] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
           Permissions.verifyPermissions(this,permissions);
       }

    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/+");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT){
            startActivityForResult(intent,Build.VERSION_CODES.KITKAT);
        }else{
            String[] type= {"image/*"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES,type);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
            startActivityForResult(intent,Constants.GALLERY);
        }
    }

    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoFile = null;
        if(takePictureIntent.resolveActivity(getPackageManager())!=null){
            try{
                photoFile = createImageFile();
            }catch (IOException e){
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        if(photoFile!=null) {
            Uri photoUri = FileProvider.getUriForFile(this, getPackageName(), photoFile);
            List<ResolveInfo> resolveInfoList = getPackageManager().queryIntentActivities(takePictureIntent, getPackageManager().MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo: resolveInfoList){

                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName,photoUri,Intent.FLAG_GRANT_WRITE_URI_PERMISSION| getIntent().FLAG_GRANT_READ_URI_PERMISSION);
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
            startActivityForResult(takePictureIntent,50);
        }

        }

    private File createImageFile() throws IOException {
        String nameFileName = "Imagen"+new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (storageDir!=null && !storageDir.exists()){
            boolean result = storageDir.mkdir();
            if(!result){
                return null;
            }
        }

        return File.createTempFile(nameFileName,".jpg",storageDir);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Resultados de la galeria
        if (requestCode == Build.VERSION_CODES.KITKAT){
            resultGalleryKitKatLess(data.getData());
        }else if (requestCode == Constants.GALLERY)
        {
            resultGalleryKitKatHigher(data);
        }
        //Resultados de la camara
        if(requestCode ==50){
            if(photoFile !=null){
                setArrayPhotos(photoFile.getPath());
            }
        }
    }

    private void resultGalleryKitKatHigher(Intent data) {
        ClipData clipData = data.getClipData();
        if(clipData != null){
            for(int i= 0; i< clipData.getItemCount();i++){
                grantUriPermission(getPackageName(),clipData.getItemAt(i).getUri(),Intent.FLAG_GRANT_READ_URI_PERMISSION);
                setArrayPhotos(clipData.getItemAt(i).getUri().toString());
            }
        }else
        {
            setArrayPhotos(data.getData().toString());
        }
    }

    private void resultGalleryKitKatLess(Uri data) {
        grantUriPermission(getPackageName(),data, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        setArrayPhotos(data.toString());
    }

    private void setArrayPhotos(String photo){
        photos.add(photo);
        photoAdadpter.setArrayPhotos(photos);
        photoAdadpter.notifyDataSetChanged();
    }
}

