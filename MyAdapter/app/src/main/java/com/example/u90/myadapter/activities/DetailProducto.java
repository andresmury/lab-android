package com.example.u90.myadapter.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.u90.myadapter.R;
import com.example.u90.myadapter.models.Producto;

public class DetailProducto extends AppCompatActivity {

    private Producto product;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_producto);
        product = (Producto) getIntent().getSerializableExtra("prodcut");
        textView.setText(product.getProductoDescripcion());
    }
}
