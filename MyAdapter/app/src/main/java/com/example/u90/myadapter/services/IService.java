package com.example.u90.myadapter.services;

import com.example.u90.myadapter.models.Producto;
import com.example.u90.myadapter.models.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IService {


    @GET("products")
    Call<ArrayList<Producto>> getProductos();

    @POST("products")
    Call<Producto> saveProducto(@Body Producto producto);

    @DELETE("products/{id}")
    Call<Boolean> deleteProducto(@Path("id") String id);

    @GET("user/auth")
    Call<User> autenticar(@Query("email")  String user, @Query("password") String password);

    @GET("user")
    Call<User> autoLogin(@Header("Authorization") String token);
}
