package com.example.u90.myadapter.repositories;

import com.example.u90.myadapter.models.User;
import com.example.u90.myadapter.services.ServicesFactory;
import com.example.u90.myadapter.models.Producto;
import com.example.u90.myadapter.services.IService;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class Repository {

    private IService iService;

    public Repository(){
          ServicesFactory servicesFactory = new ServicesFactory();
          iService = (IService) servicesFactory.getInstanceServices(IService.class);
    }

    public ArrayList<Producto> getProductos() throws IOException{

        try{
            Call<ArrayList<Producto>> call = iService.getProductos();
            Response<ArrayList<Producto>> response =  ((Call) call).execute();
            if(response.errorBody()!=null){
                throw defaultError();
            }else{
                return response.body();
            }

        }catch(IOException e){
            throw defaultError();
        }
    }

    private IOException defaultError() {
        return new IOException("Ha ocurrido un error");
    }

    public Producto saveProducto(Producto producto) throws IOException {
        try {
            Call<Producto> call = iService.saveProducto(producto);
            Response<Producto> reponse = call.execute();
            if(reponse.errorBody()!=null){
                throw defaultError();
            }else {
                return reponse.body();
            }

        }catch (IOException e){
            throw defaultError();
        }
    }

    public boolean deleteProducto(Producto producto) throws IOException {
        try {
            Call<Boolean> call = iService.deleteProducto(producto.getId());
            Response<Boolean> reponse = call.execute();
            if(reponse.errorBody()!=null){
                throw defaultError();
            }else {
                return reponse.body();
            }

        }catch (IOException e){
            throw defaultError();
        }
    }

    public User autenticar(User user) throws IOException {
        try {
            Call<User> call = iService.autenticar(user.getEmail(),user.getPassword());
            Response<User> reponse = call.execute();
            if(reponse.errorBody()!=null){
                throw defaultError();
            }else {
                return reponse.body();
            }

        }catch (IOException e){
            throw defaultError();
        }
    }

    public User autoLogin(String token) throws IOException {
        try {
            String bearerToken = "bearer:"+token;
            Call<User> call = iService.autoLogin(bearerToken);
            Response<User> reponse = call.execute();
            if(reponse.errorBody()!=null){
                throw defaultError();
            }else {
                return reponse.body();
            }

        }catch (IOException e){
            throw defaultError();
        }
    }
}
