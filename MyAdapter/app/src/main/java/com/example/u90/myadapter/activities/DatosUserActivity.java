package com.example.u90.myadapter.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.u90.myadapter.R;
import com.example.u90.myadapter.models.Producto;
import com.example.u90.myadapter.models.User;
import com.squareup.picasso.Picasso;

public class DatosUserActivity extends AppCompatActivity {

    private User user;
    private ImageView imageView;
    private TextView textViewNameUsuario;
    private TextView textViewEmailUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_user);
        user = (User) getIntent().getSerializableExtra("user");
        textViewNameUsuario= findViewById(R.id.textViewNameUsuario);
        textViewEmailUsuario= findViewById(R.id.textViewEmailUsuario);
        imageView = findViewById(R.id.imageViewUsuario);

        textViewNameUsuario.setText(user.getName());
        textViewEmailUsuario.setText(user.getEmail());
        Picasso.get().load(user.getFoto()).into(imageView);

    }
}
