package com.example.u90.myadapter.Constants;

public class Constantes {

    public final static String USER_NAME="user_name";
    public final static String USER_OBJECT="object";
    public final static String ORDER="order";
    public final static String CONFIRMACION="confirmacion";
    public final static int RESULT_OK=1;
}
