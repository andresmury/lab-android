package com.example.u90.myadapter.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.u90.myadapter.R;
import com.example.u90.myadapter.helper.Constants;
import com.example.u90.myadapter.helper.CustomSharedPreferences;
import com.example.u90.myadapter.helper.ValidateInternet;
import com.example.u90.myadapter.models.User;
import com.example.u90.myadapter.repositories.Repository;

import java.io.IOException;

import static com.example.u90.myadapter.Constants.Constantes.USER_NAME;
import static com.example.u90.myadapter.Constants.Constantes.USER_OBJECT;

public class Autenticacion extends AppCompatActivity implements TextWatcher {

    private EditText etUser;
    private EditText etPassword;
    private Button btnLogin;
    private ValidateInternet validateInternet;
    private Repository repository;
    private CustomSharedPreferences customSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticacion);
        validateInternet = new ValidateInternet(this);
        repository = new Repository();
        customSharedPreferences = new CustomSharedPreferences(this);
        etUser=findViewById(R.id.login_etUser);
        etPassword=findViewById(R.id.login_etPassword);
        btnLogin=findViewById(R.id.btnLogin);
        etUser.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
        btnLogin.setEnabled(false);
        verifityToken();

        /*btnLogin.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Autenticacion.this, DatosUserActivity.class);
            startActivity(intent);
        }
    });*/

    }

    private void verifityToken() {
        if(customSharedPreferences.getString(Constants.TOKEN)!=null){
            //Logica para ir al repositorio con el metodo autologin

        }
    }

    private void validateInternet(){
        if(validateInternet.isConnected()){
            createThreadToLoginWhithToken();
        }else{
            //toast
        }
    }

    private void createThreadToLoginWhithToken() {
         Thread thread = new Thread(new Runnable() {
             @Override
             public void run() {
                 User user= null;
                 try {
                     user = repository.autoLogin(customSharedPreferences.getString(Constants.TOKEN));
                     Intent intent = new Intent(Autenticacion.this, DatosUserActivity.class);
                     intent.putExtra("user", user);
                     startActivity(intent);
                 } catch (IOException e) {
                     e.printStackTrace();
                     showToast(e.getMessage());
                 }
             }
         }) ;
    }




    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        if(etUser.getText().toString().trim().isEmpty() ||
                etPassword.getText().toString().trim().isEmpty()){
            btnLogin.setEnabled(false);
        }else{
            btnLogin.setEnabled(true);
        }

    }

    public void ejecutarLogin(View view) {

       if(validateInternet.isConnected()){
           createThreadToLogin();
       }else{
           //toast o alert
       }


    }

    private void createThreadToLogin() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                loginInRepositoty();
            }
        });
        thread.start();
    }

 public void loginInRepositoty(){


     try {
         User user = new User();
         user.setEmail(etUser.getText().toString());
         user.setPassword(etPassword.getText().toString());
         user = repository.autenticar(user);
         customSharedPreferences.addString(Constants.TOKEN,user.getToken());
         customSharedPreferences.saveUser(Constants.USER , user);
         showToast(user.getName());
         Intent intent = new Intent(Autenticacion.this, DatosUserActivity.class);
         intent.putExtra("user", user);
         startActivity(intent);
         }catch (final IOException e) {
         showToast("Error de autenticacion");
         }

   }


    private void showToast(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(Autenticacion.this, message, Toast.LENGTH_SHORT).show();
                            }
        });

    }

}
