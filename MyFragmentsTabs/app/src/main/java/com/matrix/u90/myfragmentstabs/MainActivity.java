package com.matrix.u90.myfragmentstabs;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabs_container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.viewPager);
        tabs_container =  findViewById(R.id.tabs_container);

        loadViewPager();

        //Ejemplo Implementacion para abrir una url
        /*Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("www.google.com"));
        startActivity(intent);*/
        // fin ejemplo
    }

    private void loadViewPager() {
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabsAdapter);
        int color = ContextCompat.getColor(this, R.color.colorWhite);
        tabs_container.setSelectedTabIndicatorColor(color);
        tabs_container.setTabTextColors(ColorStateList.valueOf(color));
        tabs_container.setupWithViewPager(viewPager);
    }
}
