package com.matrix.u90.myfragmentstabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabsAdapter extends FragmentStatePagerAdapter {


    public TabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int numTab) {
        switch (numTab) {
            case 0:
                return new FragmentOne();
            case 1:
                return new FragmentTwo();
            case 2:
                return new FragmentThree();
            default:
                return new FragmentOne();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle (int position){
        switch (position) {
            case 0:  return "Estados ";
            case 1:  return "Llamadas";
            case 2:  return "Contactos";
            default: return "";
        }
    }
}
