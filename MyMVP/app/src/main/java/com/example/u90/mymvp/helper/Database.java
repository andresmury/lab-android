package com.example.u90.mymvp.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.example.u90.mymvp.dao.ContactDAO;

public class Database {
    private final Context context;
    private DatabaseHelper dbHelper;

    //Dao's
    public static ContactDAO contactDAO;


    public Database(Context context) {
        this.context = context;
    }

    public Database open(){
        try {
            dbHelper= new DatabaseHelper(context);
            SQLiteDatabase sdb = dbHelper.getWritableDatabase();
            contactDAO = new ContactDAO(sdb);
            return this;
        }catch(SQLiteException ex){
              throw  ex;
        }
    }

    public void close(){
        dbHelper.close();
    }
}
