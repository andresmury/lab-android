package com.example.u90.mymvp.interfaces;

import com.example.u90.mymvp.views.IBaseView;

public interface ICrearContactView extends IBaseView {

    void showMessageLocalContact(boolean success);
}
