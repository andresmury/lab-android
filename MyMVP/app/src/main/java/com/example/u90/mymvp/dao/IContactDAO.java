package com.example.u90.mymvp.dao;

import com.example.u90.mymvp.models.Contact;

import java.util.ArrayList;

interface IContactDAO {

  public ArrayList<Contact> fetchAllContacts();
  public Boolean createContact(Contact contacto);
}

