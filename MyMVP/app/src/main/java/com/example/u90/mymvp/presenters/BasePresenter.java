package com.example.u90.mymvp.presenters;

import com.example.u90.mymvp.helper.ValidateInternet;
import com.example.u90.mymvp.views.IBaseView;

public class BasePresenter<T extends IBaseView> {
    private ValidateInternet validateInternet;
    private T view;



    public void inject(T view , ValidateInternet validateInternet) {
        this.view = view;
        this.validateInternet = validateInternet;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }


    public T getView() {
        return view;
    }
}



