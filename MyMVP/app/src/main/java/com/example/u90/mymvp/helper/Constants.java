package com.example.u90.mymvp.helper;

public class Constants {

    public final static String TOKEN = "token";
    public final static String USER = "user";
    public static final String DATABASE_NAME = "db_contactos.db" ;
    public static final int DATABASE_VERSION = 1;
}
