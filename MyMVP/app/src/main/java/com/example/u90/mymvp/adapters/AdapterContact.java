package com.example.u90.mymvp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.u90.mymvp.R;
import com.example.u90.mymvp.models.Contact;

import java.util.ArrayList;

public class AdapterContact extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public ArrayList<Contact> listContacts;

    public AdapterContact(ArrayList<Contact> listContacts) {
        this.listContacts = listContacts;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_contact,parent,false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        Contact contacto = listContacts.get(position);
        Log.i("","contacto  tamano::"+listContacts.size());
        Log.i("","contacto  nombre ::"+contacto.getName());
        customViewHolder.textViewNombre.setText(contacto.getName());
        customViewHolder.textViewTelefono.setText(contacto.getPhone());
        customViewHolder.textViewEmpresa.setText(contacto.getCompany());
    }

    @Override
    public int getItemCount() {

        return listContacts.size();

    }

    private class CustomViewHolder extends RecyclerView.ViewHolder{

        private TextView textViewNombre;
        private TextView textViewTelefono;
        private TextView textViewEmpresa;


        public CustomViewHolder(View itemView) {
            super(itemView);
            textViewNombre=itemView.findViewById(R.id.textViewNombre);
            textViewTelefono= itemView.findViewById(R.id.textViewTelefono);
            textViewEmpresa= itemView.findViewById(R.id.textViewEmpresa);
        }
    }

}
