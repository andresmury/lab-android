package com.example.u90.mymvp.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.u90.mymvp.R;
import com.example.u90.mymvp.helper.ValidateInternet;
import com.example.u90.mymvp.presenters.BasePresenter;

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements IBaseView{

    private ValidateInternet validateInternet;
    private T presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validateInternet = new ValidateInternet(this);
    }

    public T getPresenter() {
        return presenter;
    }

    public void setPresenter(T presenter) {
        this.presenter = presenter;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    @Override
    public void showAlertDialog() {
        //TODO
    }

    @Override
    public void showToast(String mensaje) {
        Toast.makeText((Context) getPresenter().getView(), mensaje, Toast.LENGTH_SHORT).show();
    }


}
