package com.example.u90.mymvp.presenters;

import android.database.sqlite.SQLiteException;

import com.example.u90.mymvp.helper.Database;
import com.example.u90.mymvp.interfaces.IShowContacts;
import com.example.u90.mymvp.models.Contact;

import java.util.ArrayList;

public class ShowContactsPresenter  extends BasePresenter<IShowContacts>{

    public void showContactList() {
        this.getContactsThreadList();
    }

    public void getContactsThreadList(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                getContactsFromDb();
            }
        });
        t.start();
    }

    private void getContactsFromDb(){
        try{
            ArrayList<Contact> contactsList =  Database.contactDAO.fetchAllContacts();
            getView().showContacts(contactsList);
        }catch (SQLiteException e){
            e.getMessage();
            //
        }

    }
}
