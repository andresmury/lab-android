package com.example.u90.mymvp.presenters;

import com.example.u90.mymvp.helper.Database;
import com.example.u90.mymvp.interfaces.ICrearContactView;
import com.example.u90.mymvp.models.Contact;

public class CrearContactPresenter extends BasePresenter<ICrearContactView> {

       public void createContacto(String name, String phone, String factory){

        Contact contacto = new Contact(0, name, phone, factory);

        createThreadContacto(contacto);
    }



    private void createThreadContacto(final Contact contact) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createContactoLocalBcontactDB(contact);
            }
        });
        thread.start();
    }

    private void createContactoLocalBcontactDB(Contact contact) {
        boolean isSaved = false;
        try{
            isSaved = Database.contactDAO.createContact(contact);
            String msg = isSaved ? "Contacto Creado" : "Contacto no creado";
            getView().showMessageLocalContact(isSaved);
        }catch (Exception e){
            getView().showMessageLocalContact(isSaved);
        }
    }

}
