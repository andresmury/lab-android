package com.example.u90.mymvp.views;

public interface IBaseView {
    void showAlertDialog();
    void showToast(String mensaje);
}
