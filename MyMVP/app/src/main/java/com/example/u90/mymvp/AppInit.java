package com.example.u90.mymvp;

import android.app.Application;

import com.example.u90.mymvp.helper.Database;

public class AppInit extends Application {

public static Database mDb;


    @Override
    public void onCreate() {
        super.onCreate();
        openOrCreateDatabase();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mDb.close();
    }

    private void openOrCreateDatabase() {
        mDb = new Database(this);
        mDb.open();
    }
}
