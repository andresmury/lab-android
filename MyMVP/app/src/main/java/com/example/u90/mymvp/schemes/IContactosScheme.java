package com.example.u90.mymvp.schemes;

public interface IContactosScheme {

    //Datos de la tabla contactos
    String CONTACTOS_TABLE = "contactos";
    String COLUMN_ID = "id";
    String COLUMN_NAME ="name";
    String COLUMN_PHONE ="phone";
    String COLUMN_COMPANY ="company";
    String [] CONTACTOS_COLUMNS = new String []{COLUMN_ID, COLUMN_NAME, COLUMN_PHONE, COLUMN_COMPANY};

    // Sentencia creacion de la tabla contactos
    String CONTACTOS_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "+CONTACTOS_TABLE+" ( "
            + COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL "+ ", "
            + COLUMN_NAME + " TEXT NOT NULL, "
            + COLUMN_PHONE + " TEXT, "
            + COLUMN_COMPANY + " TEXT "
            +" ); ";

    //Sentencia de eliminacion de la tabla contactos
    String CONTACTOS_TABLE_DROP = "DROP TABLE IF NOT EXIST "+CONTACTOS_TABLE+" ; ";
}
