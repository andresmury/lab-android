package com.example.u90.mymvp.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.u90.mymvp.models.Contact;
import com.example.u90.mymvp.providers.DbContentProvider;
import com.example.u90.mymvp.schemes.IContactosScheme;

import java.util.ArrayList;

public class ContactDAO extends DbContentProvider implements IContactosScheme, IContactDAO {

    private Cursor cursor;
    private ContentValues contentValues;
    private final static String TAG = "ContactDAO";



    public ContactDAO(SQLiteDatabase mDb) {
        super(mDb);
    }

    @Override
    public ArrayList<Contact> fetchAllContacts() {
        ArrayList<Contact> contactosList = new ArrayList<>();
        cursor = query(CONTACTOS_TABLE,CONTACTOS_COLUMNS,null , null, COLUMN_NAME);

        if(cursor !=null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                Contact contact  = cursorToEntity(cursor);
                cursor.moveToNext();
                contactosList.add(contact);
            }

        }

        return contactosList;
    }


    @Override
    public Boolean createContact(Contact contacto) {
        setContentValuesContact(contacto);
        try {
            return insert(CONTACTOS_TABLE,getContenValues())>0;
        }catch (SQLException ex){
            Log.e(TAG,ex.getMessage());
            return false;
        }
    }

    private ContentValues getContenValues() {
        return contentValues;
    }

    private void setContentValuesContact(Contact contacto) {
        contentValues = new ContentValues();
       // contentValues.put(COLUMN_ID,contacto.getId() );
        contentValues.put(COLUMN_NAME,contacto.getName() );
        contentValues.put(COLUMN_PHONE,contacto.getPhone() );
        contentValues.put(COLUMN_COMPANY,contacto.getCompany() );
    }


    @Override
    protected Contact cursorToEntity(Cursor cursor) {
        Contact contact = new Contact();
        if (cursor.getColumnIndex(COLUMN_ID) != -1 ){

            contact.setId(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID)));
         }
         if(cursor.getColumnIndex(COLUMN_NAME) != -1){
             contact.setName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)));
         }

        if( cursor.getColumnIndex(COLUMN_PHONE) != -1){
            contact.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PHONE)));
        }

        if( cursor.getColumnIndex(COLUMN_COMPANY) != -1){
            contact.setCompany(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_COMPANY)));
        }

        return contact;
    }
}
