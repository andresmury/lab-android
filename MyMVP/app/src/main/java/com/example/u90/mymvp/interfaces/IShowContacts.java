package com.example.u90.mymvp.interfaces;

import com.example.u90.mymvp.models.Contact;
import com.example.u90.mymvp.views.IBaseView;

import java.util.ArrayList;

public interface IShowContacts extends IBaseView {

    void showContacts(ArrayList<Contact> contactsList);
}
