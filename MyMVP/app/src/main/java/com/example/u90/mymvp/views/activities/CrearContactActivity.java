package com.example.u90.mymvp.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.u90.mymvp.R;
import com.example.u90.mymvp.interfaces.ICrearContactView;
import com.example.u90.mymvp.presenters.CalcularEdadPresenter;
import com.example.u90.mymvp.presenters.CrearContactPresenter;
import com.example.u90.mymvp.views.BaseActivity;

public class CrearContactActivity extends BaseActivity<CrearContactPresenter> implements ICrearContactView {

    private EditText nombre;
    private EditText telefono;
    private EditText empresa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_contact);
        setComponents();
        setPresenter(new CrearContactPresenter());
        getPresenter().inject(this, getValidateInternet());

    }

    private void setComponents() {
        nombre = findViewById(R.id.nom_contacto);
        telefono = findViewById(R.id.tel_contacto);
        empresa = findViewById(R.id.emp_contacto);
    }

    public void showContacts(View view) {
        Intent intent = new Intent(this, ShowContactsActivity.class);
        startActivity(intent);
        //finish();
    }

    public void createContact(View view) {
        getPresenter().createContacto(nombre.getText().toString(),
                telefono.getText().toString(), empresa.getText().toString());
    }

    @Override
    public void showMessageLocalContact(final boolean success) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String msg = success ? "Contacto creado" : "Contacto no creado";
                Toast.makeText(CrearContactActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
