package com.example.u90.mymvp.providers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class DbContentProvider {
    public SQLiteDatabase mDb;
    protected abstract <T> T cursorToEntity (Cursor cursor);

    public DbContentProvider(SQLiteDatabase mDb) {
        this.mDb = mDb;
    }

    public long insert(String tableName,  ContentValues values){

        return mDb.insert(tableName,null,values);
    }

    private int update (String tableName, ContentValues values, String whereClause, String [] selectionArgs){
       return  mDb.update(tableName,values,whereClause,selectionArgs);
    }

     public int delete (String tableName, String whereClause, String [] selectionArgs){

        return mDb.delete(tableName, whereClause, selectionArgs);
      }

      public Cursor query(String tableName, String [] columns, String whereClause, String [] selectionArgs, String sortOrder){

        return mDb.query(tableName,columns,whereClause,selectionArgs,null,null,sortOrder);
      }

    public Cursor query(String tableName, String [] columns, String whereClause, String [] selectionArgs, String sortOrder, String limit){

        return mDb.query(tableName,columns,whereClause,selectionArgs,null,null,sortOrder, limit);
    }

    public Cursor query(String tableName, String [] columns, String whereClause, String [] selectionArgs,String groupBy, String having, String sortOrder, String limit){

        return mDb.query(tableName,columns,whereClause,selectionArgs,groupBy,having,sortOrder, limit);
    }






}
