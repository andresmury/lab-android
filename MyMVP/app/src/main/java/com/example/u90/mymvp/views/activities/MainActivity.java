package com.example.u90.mymvp.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.u90.mymvp.interfaces.IMainView;
import com.example.u90.mymvp.presenters.MainPresenter;
import com.example.u90.mymvp.views.BaseActivity;
import com.example.u90.mymvp.views.IBaseView;
import com.example.u90.mymvp.R;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainView {

    private EditText numberOne;
    private EditText numberTwo;
    private Button calculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setComponents();
        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet());
    }

    private void setComponents() {
        numberOne = findViewById(R.id.login_et1);
        numberTwo = findViewById(R.id.login_et2);

    }

    public void calculate(View view) {
       // int result = getPresenter().sumCalculate(Integer.parseInt(numberOne.getText().toString()),
       //         Integer.parseInt(numberTwo.getText().toString()));
        //showResult( result);

       getPresenter().calculate(Integer.parseInt(numberOne.getText().toString()),
                Integer.parseInt(numberTwo.getText().toString()));

    }

    @Override
    public void showResult(int i) {
        showToast( String.valueOf(i));
    }


}
