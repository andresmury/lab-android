package com.example.u90.mymvp.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.u90.mymvp.R;
import com.example.u90.mymvp.adapters.AdapterContact;
import com.example.u90.mymvp.interfaces.IShowContacts;
import com.example.u90.mymvp.models.Contact;
import com.example.u90.mymvp.presenters.ShowContactsPresenter;
import com.example.u90.mymvp.views.BaseActivity;

import java.util.ArrayList;

public class ShowContactsActivity  extends BaseActivity<ShowContactsPresenter> implements IShowContacts {

    private AdapterContact adapterContacts;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_contacts);
        recyclerView = findViewById(R.id.recycler_view_contacts);
        setPresenter(new ShowContactsPresenter());
        getPresenter().inject(this, getValidateInternet());
        getPresenter().showContactList();
    }

    @Override
    public void showContacts(ArrayList<Contact> contactsList) {

        adapterContacts  = new AdapterContact(contactsList);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterContacts);

    }
}
