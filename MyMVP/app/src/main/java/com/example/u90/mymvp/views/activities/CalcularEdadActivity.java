package com.example.u90.mymvp.views.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.example.u90.mymvp.R;
import com.example.u90.mymvp.interfaces.ICalcularEdadView;
import com.example.u90.mymvp.presenters.CalcularEdadPresenter;
import com.example.u90.mymvp.presenters.MainPresenter;
import com.example.u90.mymvp.views.BaseActivity;

public class CalcularEdadActivity extends BaseActivity<CalcularEdadPresenter> implements ICalcularEdadView {

    private EditText fecNacimiento;
    private Button calculateEdad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcular_edad);
        setComponents();
        setPresenter(new CalcularEdadPresenter());
        getPresenter().inject(this, getValidateInternet());
    }

    public void calcularEdad(View view) {
        getPresenter().calculateEdad(fecNacimiento);
    }

    private void setComponents() {
        fecNacimiento = findViewById(R.id.fecNacimiento);

    }
}
