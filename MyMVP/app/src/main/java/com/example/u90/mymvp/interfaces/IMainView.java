package com.example.u90.mymvp.interfaces;

import com.example.u90.mymvp.views.IBaseView;

public interface IMainView extends IBaseView {
    void showResult(int i);
}
