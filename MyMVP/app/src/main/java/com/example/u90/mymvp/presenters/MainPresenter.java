package com.example.u90.mymvp.presenters;

import com.example.u90.mymvp.interfaces.IMainView;

public class MainPresenter extends BasePresenter<IMainView> {

    public int sumCalculate(int one, int two) {
        return one + two;
    }

    public void calculate (int one , int two){
        getView().showToast(String.valueOf(one+two));
    }
}
